import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { Account, AccountAuth } from "../interfaces/account.interface";

interface AccountState extends Account {
  auth?: AccountAuth;
}

const initialState: AccountState = {};

const loginTypeReducer = createSlice({
  name: "account",
  initialState,
  reducers: {
    setAccountDetails: (state, action) => {
      const {
        userId,
        firstName,
        lastName,
        emailAddress,
        image,
        accountNumber,
      } = action.payload;
      state.userId = userId;
      state.firstName = firstName;
      state.lastName = lastName;
      state.emailAddress = emailAddress;
      state.image = image;
      state.accountNumber = accountNumber;
    },
    setAuth: (state, action: PayloadAction<AccountAuth>) => {
      const { id, accessToken, masterKey } = action.payload;
      state.auth = { id, accessToken, masterKey };
    },
  },
});

export const { setAccountDetails, setAuth } = loginTypeReducer.actions;
export default loginTypeReducer.reducer;
