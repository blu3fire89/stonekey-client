import { createSlice } from "@reduxjs/toolkit";
import { LoginType } from "../interfaces/loginType.interface";

export interface LoginTypes {
  list: LoginType[];
}

const initialState: LoginTypes = {
  list: [],
};

const loginTypeReducer = createSlice({
  name: "loginTypes",
  initialState,
  reducers: {
    setLoginTypes: (state, action) => {
      state.list = action.payload;
    },
  },
});

export const { setLoginTypes } = loginTypeReducer.actions;
export default loginTypeReducer.reducer;
