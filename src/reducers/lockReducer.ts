import { createSlice } from "@reduxjs/toolkit";
import { Lock } from "../interfaces/lock.interface";
import { Category } from "../interfaces/category.interface";

export interface LockState {
  list: Lock[];
  filteredList?: Lock[];
  isOpen: boolean;
  selected?: Lock;
  modalType: "add" | "view" | "edit";
  filterModal: {
    isOpen: boolean;
  };
  isFiltered: boolean;
  filter: {
    sort: string;
    categories: Category[];
    searchText?: string;
  };
}

const initialState: LockState = {
  list: [],
  filteredList: [],
  isOpen: false,
  selected: null,
  modalType: "add",
  filterModal: {
    isOpen: false,
  },
  isFiltered: false,
  filter: {
    sort: "updatedAt-desc",
    categories: [],
    searchText: "",
  },
};

const lockReducer = createSlice({
  name: "locks",
  initialState,
  reducers: {
    handleModal: (state, action) => {
      const { isOpen, modalType } = action.payload;
      state.isOpen = isOpen;
      state.modalType = modalType;
    },
    handleSelectCard: (state, action) => {
      state.isOpen = true;
      state.modalType = "view";
      state.selected = action.payload;
    },
    handleFilterModal: (state, action) => {
      const { isOpen } = action.payload;
      state.filterModal.isOpen = isOpen;
    },
    setLocks: (state, action) => {
      state.list = action.payload;
      state.filteredList = action.payload;
    },
    setFilteredLocks: (state, action) => {
      state.filteredList = action.payload;
      state.isFiltered = state.filteredList.length !== state.list.length;
    },
    setFilter: (state, action) => {
      const { sort, categories } = action.payload;
      state.filter.sort = sort;
      state.filter.categories = categories;
    },
    setSearchFilter: (state, action) => {
      const { searchText } = action.payload;
      state.filter.searchText = searchText;
    },
  },
});

export const {
  handleModal,
  handleSelectCard,
  handleFilterModal,
  setLocks,
  setFilteredLocks,
  setFilter,
  setSearchFilter,
} = lockReducer.actions;
export default lockReducer.reducer;
