import { PayloadAction, createSlice } from "@reduxjs/toolkit";

export interface GlobalState {
  deleteModal: {
    isOpen: boolean;
  };
  logoutModal: {
    isOpen: boolean;
  };
  error?: {
    type?: "expired-jwt" | "no-token";
    text?: string;
  };
  isOnline: boolean;
}

const initialState: GlobalState = {
  deleteModal: {
    isOpen: false,
  },
  logoutModal: {
    isOpen: false,
  },
  error: null,
  isOnline: false,
};

const globalReducer = createSlice({
  name: "global",
  initialState,
  reducers: {
    handleDeleteModal: (state, action: PayloadAction<{ isOpen: boolean }>) => {
      const { isOpen } = action.payload;
      state.deleteModal.isOpen = isOpen;
    },
    handleLogoutModal: (state, action: PayloadAction<{ isOpen: boolean }>) => {
      const { isOpen } = action.payload;
      state.logoutModal.isOpen = isOpen;
    },
    setGlobalError: (
      state,
      action: PayloadAction<{
        type?: "expired-jwt" | "no-token";
        text?: string;
      }>
    ) => {
      state.error = action.payload;
    },
    setIsOnline: (state, action: PayloadAction<boolean>) => {
      state.isOnline = action.payload;
    },
  },
});

export const {
  handleDeleteModal,
  handleLogoutModal,
  setGlobalError,
  setIsOnline,
} = globalReducer.actions;
export default globalReducer.reducer;
