import CryptoJS from "crypto-js";
import { getAuthDetails } from "../api/db/dexieApi";

export const ecryptData = async (str: string) => {
  const { masterKey } = await getAuthDetails();
  return CryptoJS.AES.encrypt(str, masterKey).toString() ?? "";
};
export const decryptData = async (str: string) => {
  const { masterKey } = await getAuthDetails();
  return CryptoJS.AES.decrypt(str, masterKey).toString(CryptoJS.enc.Utf8) ?? "";
};
