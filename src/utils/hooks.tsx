import { getCategoriesApi, getLocksApi } from "../api/api";
import { setLocks } from "../reducers/lockReducer";
import { store } from "../store";
import { setCategories } from "../reducers/categoryReducer";
import { decryptData } from "./hashPassword";
import { checkOnline } from "./utils";
import { setIsOnline } from "../reducers/globalReducer";

export const isHashed = (str: string) =>
  typeof str === "string" ? str.startsWith("U2FsdGVkX1") : false;

// TODO for check on error
export const refreshLocks = async () => {
  const reduxStore = store;
  return await getLocksApi().then(async (res) => {
    const newRes = await Promise.all(
      await res?.map(async (lock) => {
        return {
          ...lock,
          // ecrypted data
          password: (await decryptData(lock?.password)) ?? "",
          username: isHashed(lock?.username)
            ? await decryptData(lock?.username)
            : lock?.username,
          description: isHashed(lock?.description)
            ? await decryptData(lock?.description)
            : lock?.description,
        };
      })
    );
    reduxStore.dispatch(setLocks(newRes));
  });
};

export const refreshCategories = async () => {
  const reduxStore = store;
  return await getCategoriesApi().then((res) =>
    reduxStore.dispatch(setCategories(res))
  );
};

export const refreshOnlineState = async () => {
  const reduxStore = store;

  return await checkOnline().then((res: boolean) =>
    reduxStore.dispatch(setIsOnline(res))
  );
};
