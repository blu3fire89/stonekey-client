import { Lock } from "../interfaces/lock.interface";
const DEFAULT_TIME_OUT = 5000; // MS

const getRandomString = () => {
  return Math.random().toString(36).substring(2, 15);
};

export async function checkOnline() {
  if (!sessionStorage?.getItem("onlineMode")) return false;
  if (!window.navigator.onLine) return false;

  // avoid CORS errors with a request to your own origin
  const ownWebSite = "https://sleipnir-briankarlsayens-projects.vercel.app";
  // const ownWebSite = "https://openlibrary.org/search.json";
  const url = new URL(ownWebSite);

  // random value to prevent cached responses
  url.searchParams.set("rand", getRandomString());

  try {
    const controller = new AbortController();
    const { signal } = controller;
    const timeoutId = setTimeout(() => controller.abort(), DEFAULT_TIME_OUT);
    const response = await fetch(url.toString(), { method: "HEAD", signal });
    clearTimeout(timeoutId);
    console.log("response", response);
    if (!response?.ok) sessionStorage.removeItem("onlineMode");
    return response.ok;
  } catch (error) {
    console.log("err", error);
    return false;
  }
}

function compareArrays(arr1, arr2) {
  const arr1Set = new Set(arr1);
  for (const item of arr2) {
    if (!arr1Set.has(item)) {
      return false;
    }
  }
  return true;
}

export const filterLocks = (list: Lock[], type) => {
  const convertedList = list.map((item) => {
    return {
      ...item,
      convertedUpdatedAt: new Date(item.updatedAt).getTime(),
    };
  });
  const selectedCategories = type.categories.map((category) => category._id);
  // filter locks with selected categories
  const filterListWithCategories = convertedList.filter((item) =>
    compareArrays(item.categoryArr, selectedCategories)
  );
  switch (type.sort) {
    case "updatedAt-desc":
      return filterListWithCategories.sort(
        (a, b) => b.convertedUpdatedAt - a.convertedUpdatedAt
      );
    case "updatedAt-asc":
      return filterListWithCategories.sort(
        (a, b) => a.convertedUpdatedAt - b.convertedUpdatedAt
      );
    case "name-asc":
      return filterListWithCategories.sort((a, b) => {
        const nameA = a.title.toUpperCase(); // ignore upper and lowercase
        const nameB = b.title.toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }

        // names must be equal
        return 0;
      });
    case "name-desc":
      return filterListWithCategories.sort((a, b) => {
        const nameA = a.title.toUpperCase(); // ignore upper and lowercase
        const nameB = b.title.toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
          return 1;
        }
        if (nameA > nameB) {
          return -1;
        }

        // names must be equal
        return 0;
      });
    default:
      return filterListWithCategories;
  }
};
