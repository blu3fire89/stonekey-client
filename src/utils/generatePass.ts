function randrange(min: number, max: number) {
  var range = max - min;
  if (range <= 0) {
    throw new Error("max must be larger than min");
  }
  var requestBytes = Math.ceil(Math.log2(range) / 8);
  if (!requestBytes) {
    // No randomness required
    return min;
  }
  var maxNum = Math.pow(256, requestBytes);
  var ar = new Uint8Array(requestBytes);

  while (true) {
    window.crypto.getRandomValues(ar);

    var val = 0;
    for (var i = 0; i < requestBytes; i++) {
      val = (val << 8) + ar[i];
    }

    if (val < maxNum - (maxNum % range)) {
      return min + (val % range);
    }
  }
}

function shuffleArray(array: string[]) {
  for (let i = array.length - 1; i > 0; i--) {
    let j = randrange(0, i);
    let temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
  return array;
}
// 1 number, 1 lowerCase, 1 upperCase, 1 special, n etc.
export const randomString = (length: number) => {
  const specialChars = "!@#$%^&*()_+{}<>?[]~";
  const numberChars = "0123456789";
  const upperChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  const lowerChars = "abcdefghijklmnopqrstuvwxyz";
  const allChars = numberChars + upperChars + lowerChars;
  let randPasswordArray = Array(length);
  randPasswordArray[0] = numberChars;
  randPasswordArray[1] = upperChars;
  randPasswordArray[2] = lowerChars;
  randPasswordArray[3] = specialChars;
  randPasswordArray = randPasswordArray.fill(allChars, 4);
  return shuffleArray(
    randPasswordArray.map(function (x) {
      return x[randrange(0, x.length)];
    })
  ).join("");
};
