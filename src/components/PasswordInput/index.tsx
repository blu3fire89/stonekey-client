import { Visibility, VisibilityOff, CopyAll } from "@mui/icons-material";
import {
  IconButton,
  InputAdornment,
  Popover,
  TextFieldProps,
  Typography,
} from "@mui/material";
import { useState } from "react";
import BasicTextField from "../BasicTextField";

type PasswordInputProps = {
  showcopybtn?: string;
} & TextFieldProps;

function PasswordInput(props: PasswordInputProps) {
  const [showPassword, setShowPassword] = useState(false);
  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);
  const open = Boolean(anchorEl);
  const handleCopyText = (event) => {
    setAnchorEl(event.currentTarget);
    navigator.clipboard.writeText(props?.value as string);
    setTimeout(() => {
      setAnchorEl(null);
    }, 1500);
  };
  return (
    <BasicTextField
      {...props}
      type={showPassword ? "text" : "password"}
      InputProps={{
        endAdornment: (
          <InputAdornment position="end" sx={{ gap: 1 }}>
            {props?.showcopybtn && (
              <IconButton onClick={handleCopyText} edge="end">
                <CopyAll />
              </IconButton>
            )}
            <IconButton
              aria-label="toggle password visibility"
              onClick={handleClickShowPassword}
              edge="end"
            >
              {showPassword ? <VisibilityOff /> : <Visibility />}
            </IconButton>
            <Popover
              id="copy-popover"
              open={open}
              anchorEl={anchorEl}
              anchorOrigin={{
                vertical: "top",
                horizontal: "left",
              }}
              transformOrigin={{
                vertical: "bottom",
                horizontal: "left",
              }}
            >
              <Typography sx={{ p: 1.5 }}>Copy</Typography>
            </Popover>
          </InputAdornment>
        ),
      }}
    />
  );
}

export default PasswordInput;
