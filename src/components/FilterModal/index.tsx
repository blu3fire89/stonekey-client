import { Close, EditNoteSharp } from "@mui/icons-material";
import {
  Autocomplete,
  Box,
  Button,
  Grid,
  IconButton,
  Modal,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../store";
import {
  handleFilterModal,
  setFilter,
  setFilteredLocks,
} from "../../reducers/lockReducer";
import BasicSelect from "../BasicSelect";
import { sortTypeList } from "./data";
import { useNavigate } from "react-router-dom";
import { filterLocks } from "../../utils/utils";
import BasicTextField from "../BasicTextField";

function FilterModal() {
  const lockState = useSelector((state: RootState) => state.lock);
  const { list: categoryList } = useSelector(
    (state: RootState) => state.category
  );
  const navigate = useNavigate();
  const open = lockState?.filterModal?.isOpen;

  const dispatch = useDispatch();
  const handleClose = () => {
    dispatch(handleFilterModal({ isOpen: false }));
  };

  const [input, setInput] = useState({
    sort: "updatedAt-desc",
    categories: [],
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    const filteredLocks = filterLocks(lockState.list, input);
    const filterByText = filteredLocks.filter((item) =>
      item.title.toLowerCase().match(lockState.filter.searchText.toLowerCase())
    );
    dispatch(setFilteredLocks(filterByText));
    dispatch(setFilter(input));
  };

  const updateField = (e) => {
    setInput({
      ...input,
      [e.target.name]: e.target.value,
    });
  };

  const handleCategoriesChange = (_e, newVal) => {
    setInput({
      ...input,
      categories: newVal,
    });
  };

  const openCategoryModal = () => {
    // dispatch(handleCategoryModal({ isOpen: true }));
    dispatch(handleFilterModal({ isOpen: false }));
    navigate("/lock-manager/category");
  };

  useEffect(() => {
    return () => setInput(lockState.filter);
  }, [open]);

  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box
        sx={{
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
          width: "90%",
          maxWidth: 600,
          bgcolor: "background.paper",
          p: 2,
          borderRadius: "5px",
        }}
      >
        <Box
          pb={2}
          display="flex"
          justifyContent="space-between"
          alignItems="center"
        >
          <Typography variant="h5">Filter Locks</Typography>
          <IconButton onClick={handleClose}>
            <Close />
          </IconButton>
        </Box>
        <FilterModalForm
          handleSubmit={handleSubmit}
          input={input}
          updateField={updateField}
          handleCategoriesChange={handleCategoriesChange}
          openCategoryModal={openCategoryModal}
          categoryList={categoryList}
        />
      </Box>
    </Modal>
  );
}

const FilterModalForm = ({
  handleSubmit,
  input,
  updateField,
  handleCategoriesChange,
  openCategoryModal,
  categoryList,
}) => {
  return (
    <form onSubmit={handleSubmit}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <BasicSelect
            list={sortTypeList}
            name="sort"
            label="Sort"
            variant="outlined"
            fullWidth
            value={input.sort}
            onChange={updateField}
          />
        </Grid>
        <Grid item xs={12}>
          <Box display="flex" gap={2} alignItems="center">
            <Autocomplete
              fullWidth
              multiple
              options={categoryList}
              getOptionLabel={(option) => option.title}
              onChange={handleCategoriesChange}
              value={input.categories}
              renderInput={(params) => (
                <BasicTextField
                  {...params}
                  variant="outlined"
                  label="Select Categories"
                />
              )}
            />
            <Box>
              <IconButton onClick={openCategoryModal}>
                <EditNoteSharp />
              </IconButton>
            </Box>
          </Box>
        </Grid>
        <Grid item xs={12}>
          <Button type="submit" variant="contained" color="primary" fullWidth>
            Submit
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default FilterModal;
