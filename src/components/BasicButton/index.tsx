import {
  Button,
  ButtonProps,
  CircularProgress,
  useMediaQuery,
} from "@mui/material";
import theme from "../../theme";

interface BasicButtonProps {
  isLoading?: boolean;
}

function BasicButton({ isLoading, ...props }: BasicButtonProps & ButtonProps) {
  const mobileSize = useMediaQuery(theme.breakpoints.down("lg"));

  return (
    <Button
      {...props}
      disabled={props?.disabled || isLoading}
      size={mobileSize ? "small" : "medium"}
    >
      {props.children}
      {isLoading && (
        <CircularProgress
          size={24}
          sx={{
            color: "primary",
            position: "absolute",
            top: "50%",
            left: "50%",
            marginTop: "-12px",
            marginLeft: "-12px",
          }}
        />
      )}
    </Button>
  );
}

export default BasicButton;
