// Navbar.js

import { useEffect, useState } from "react";
import {
  AppBar,
  Toolbar,
  Typography,
  Button,
  Drawer,
  List,
  ListItem,
  ListItemText,
  useMediaQuery,
  Box,
  IconButton,
  ListItemButton,
  Tooltip,
} from "@mui/material";
import { Download, Logout, Menu, Settings, Refresh } from "@mui/icons-material";
import theme from "../../theme";
import { useNavigate } from "react-router-dom";
import LogoutModal from "../LogoutModal";
import { useDispatch, useSelector } from "react-redux";
import { handleLogoutModal } from "../../reducers/globalReducer";
import { CSVDownload } from "react-csv";
import { decryptData } from "../../utils/hashPassword";
import { downloadLockApi } from "../../api/api";
import { isHashed } from "../../utils/hooks";
import Logo from "../../assets/logox64.png";
import { RootState } from "../../store";
type SelectedItems = "lock-manager" | "notes";

const Navbar = ({ reload, loading }) => {
  const [drawerOpen, setDrawerOpen] = useState(false);
  const [csvData, setCsvData] = useState(null);

  const toggleDrawer = (open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setDrawerOpen(open);
  };

  const desktopSize = useMediaQuery(theme.breakpoints.up("md"));

  const navigate = useNavigate();

  const [selectedItem, setSelectedItem] =
    useState<SelectedItems>("lock-manager");

  const handleItemClick = (itemName: SelectedItems) => {
    setSelectedItem(itemName);
    navigate(itemName);
  };

  const handleLogoClick = () => navigate("/lock-manager");

  const navbarItems = [
    {
      text: "Lock Manager",
      value: "lock-manager",
    },
    {
      text: "Secured Notes",
      value: "notes",
    },
  ];

  const dispatch = useDispatch();

  const handleOpenLogoutModal = () => {
    dispatch(handleLogoutModal({ isOpen: true }));
  };

  const handleDownload = async () => {
    let response = await downloadLockApi();
    if (response.data.length > 1) {
      const encryptedList = ["password", "username", "description"];
      await Promise.all(
        await encryptedList.map(async (item) => {
          const responseIdx = response.data[0].findIndex((idx) => idx === item);
          for (let i = 1; i < response.data.length; i++) {
            if (isHashed(response.data[i][responseIdx])) {
              const strVal = await decryptData(response.data[i][responseIdx]);
              if (strVal.indexOf(`"`) > -1) {
                response.data[i][responseIdx] = strVal.replaceAll(`"`, `'`);
              } else {
                response.data[i][responseIdx] = strVal;
              }
            }
            // } else {
            //   response.data[i][responseIdx] = "null";
            // }
          }
        })
      );
    }
    console.log("download");
    setCsvData(response.data);
  };

  useEffect(() => {
    setCsvData(null); // reload download function
  }, [csvData]);

  return (
    <div>
      <LogoutModal description="You are about to logout, do you want to continue?" />
      <AppBar position="static">
        {desktopSize ? (
          <DesktopView
            navigate={navigate}
            handleItemClick={handleItemClick}
            selectedItem={selectedItem}
            handleLogoClick={handleLogoClick}
            navbarItems={navbarItems}
            handleOpenLogoutModal={handleOpenLogoutModal}
            handleDownload={handleDownload}
            csvData={csvData}
            reload={reload}
            loading={loading}
          />
        ) : (
          <MobileView
            toggleDrawer={toggleDrawer}
            navigate={navigate}
            handleLogoClick={handleLogoClick}
            handleOpenLogoutModal={handleOpenLogoutModal}
            handleDownload={handleDownload}
            csvData={csvData}
            reload={reload}
            loading={loading}
          />
        )}
      </AppBar>

      {!desktopSize && (
        <Drawer anchor="top" open={drawerOpen} onClose={toggleDrawer(false)}>
          <List>
            {navbarItems.map((item) => (
              <ListItem key={item.value} onClick={toggleDrawer(false)}>
                <ListItemButton
                  onClick={() => handleItemClick(item.value as SelectedItems)}
                >
                  <ListItemText primary={item.text} />
                </ListItemButton>
              </ListItem>
            ))}
          </List>
        </Drawer>
      )}
    </div>
  );
};

const DesktopView = ({
  navigate,
  selectedItem,
  handleItemClick,
  handleLogoClick,
  navbarItems,
  handleOpenLogoutModal,
  handleDownload,
  csvData,
  reload,
  loading,
}) => {
  return (
    <Toolbar>
      <Box
        display="flex"
        width="100%"
        alignItems="center"
        justifyContent="center"
      >
        <Box display="flex" maxWidth="lg" width="100%">
          <Box display="flex" flexGrow={1} alignItems="center">
            <Box display="flex" gap={0.5}>
              <img src={Logo} width={"25px"} />
              <Typography
                variant="h6"
                component="div"
                onClick={handleLogoClick}
                sx={{ cursor: "pointer" }}
              >
                Stonekey
              </Typography>
            </Box>
            <Box display="flex" flexDirection="row" gap={2} pl={4}>
              {navbarItems.map((item, index) => (
                <Typography
                  key={index}
                  className={`navbar-item ${
                    selectedItem === item.value ? "selected" : ""
                  }`}
                  onClick={() => handleItemClick(item.value)}
                  sx={{ cursor: "pointer" }}
                >
                  {item.text}
                </Typography>
              ))}
            </Box>
          </Box>
          <LeftNavbar
            navigate={navigate}
            handleOpenLogoutModal={handleOpenLogoutModal}
            handleDownload={handleDownload}
            csvData={csvData}
            reload={reload}
            loading={loading}
          />
        </Box>
      </Box>
    </Toolbar>
  );
};

const MobileView = ({
  toggleDrawer,
  navigate,
  handleLogoClick,
  handleOpenLogoutModal,
  handleDownload,
  csvData,
  reload,
  loading,
}) => {
  return (
    <Toolbar>
      <Button color="inherit" onClick={toggleDrawer(true)}>
        <Menu />
      </Button>
      <Box display="flex" gap={0.5} flexGrow={1}>
        <img src={Logo} width={"20px"} />
        <Typography
          variant="body1"
          fontWeight={600}
          component="div"
          onClick={handleLogoClick}
          sx={{ cursor: "pointer" }}
        >
          Stonekey
        </Typography>
      </Box>
      <LeftNavbar
        navigate={navigate}
        handleOpenLogoutModal={handleOpenLogoutModal}
        handleDownload={handleDownload}
        csvData={csvData}
        reload={reload}
        loading={loading}
      />
    </Toolbar>
  );
};

const OfflineIndicator = ({ reload, loading }) => {
  const { isOnline } = useSelector((state: RootState) => state.global);
  const color = () => {
    if (!isOnline) return "#E97338";
    return "#3B9E62";
  };

  return (
    <IconButton color="secondary" onClick={reload}>
      {loading ? (
        <Refresh
          className="red"
          sx={{
            cursor: "pointer",
            animation: "spin 1.2s linear infinite",
            "@keyframes spin": {
              "100%": {
                transform: "rotate(360deg)",
              },
              "0%": {
                transform: "rotate(0deg)",
              },
            },
          }}
        />
      ) : (
        <Refresh
          className="red"
          sx={{
            cursor: "pointer",
          }}
        />
      )}
    </IconButton>
  );
  return (
    <Box display="flex" alignItems="center" pr={1}>
      <Tooltip title={isOnline ? "Online" : "Offline"}>
        <Box display="flex" alignItems="center">
          <Box
            border="2px solid black"
            height="18px"
            width="18px"
            bgcolor={color()}
            borderRadius={100}
          ></Box>
        </Box>
      </Tooltip>
    </Box>
  );
};

const LeftNavbar = ({
  navigate,
  handleOpenLogoutModal,
  handleDownload,
  csvData,
  reload,
  loading,
}) => {
  return (
    <Box display="flex">
      <OfflineIndicator reload={reload} loading={loading} />
      <Tooltip title="Download">
        <IconButton color="secondary" onClick={handleDownload}>
          <Download sx={{ cursor: "pointer" }} />
          {csvData && (
            <CSVDownload
              // ref={csvDownloadRef}
              data={csvData}
            />
          )}
        </IconButton>
      </Tooltip>
      <Tooltip title="Settings">
        <IconButton color="secondary" onClick={() => navigate("settings")}>
          <Settings sx={{ cursor: "pointer" }} />
        </IconButton>
      </Tooltip>
      <Tooltip title="Logout">
        <IconButton color="secondary" onClick={handleOpenLogoutModal}>
          <Logout sx={{ cursor: "pointer" }} />
        </IconButton>
      </Tooltip>
    </Box>
  );
};

export default Navbar;
