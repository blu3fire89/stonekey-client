import {
  Badge,
  Box,
  Button,
  Grid,
  IconButton,
  InputAdornment,
  Paper,
  Typography,
} from "@mui/material";
import LockCard from "../LockCard";
import { Add, Close, Tune } from "@mui/icons-material";
import {
  handleFilterModal,
  handleModal,
  setFilteredLocks,
  setSearchFilter,
} from "../../reducers/lockReducer";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../store";
import { useEffect } from "react";
import { filterLocks } from "../../utils/utils";
import { Lock } from "../../interfaces/lock.interface";
import BasicTextField from "../BasicTextField";

interface LockListParams {
  locks: Lock[];
}

function LockList({ locks }: LockListParams) {
  const dispatch = useDispatch();
  const lockState = useSelector((state: RootState) => state.lock);
  const { isOnline } = useSelector((state: RootState) => state.global);

  const handleAddButton = () => {
    dispatch(handleModal({ isOpen: !lockState.isOpen, modalType: "add" }));
  };

  const handleFilter = () => {
    // navigate("/lock-manager/category");
    dispatch(handleFilterModal({ isOpen: true }));
  };

  const isFiltered = lockState.isFiltered;
  const text = lockState.filter.searchText;
  const clearInput = () => {
    dispatch(setSearchFilter({ searchText: "" }));
    dispatch(setFilteredLocks(lockState.list));
  };

  const handleInputChange = (e) => {
    dispatch(setSearchFilter({ searchText: e.target.value }));
  };

  const filterBySearch = () => {
    const filter = lockState.filter;
    const lockList = filterLocks(lockState.list, filter);
    const filteredLocks = lockList.filter((item) =>
      item.title.toLowerCase().match(lockState.filter.searchText.toLowerCase())
    );
    dispatch(setFilteredLocks(filteredLocks));
  };

  function searchFilter() {
    return (
      <BasicTextField
        fullWidth
        placeholder="Search"
        size="small"
        value={text}
        onChange={handleInputChange}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end" sx={{ gap: 1 }}>
              {text && (
                <IconButton edge="end" size="small" onClick={clearInput}>
                  <Close />
                </IconButton>
              )}
            </InputAdornment>
          ),
        }}
      />
    );
  }

  const lockListHeader = () => {
    return (
      <Grid container spacing={2}>
        <Grid item xs={12} md={8}>
          {searchFilter()}
        </Grid>
        <Grid item xs={12} md={4}>
          <Box
            display="flex"
            gap={1}
            alignItems="center"
            justifyContent="space-between"
          >
            <Typography fontWeight={500}>
              Saved Password({lockState?.filteredList?.length})
            </Typography>
            <Box display="flex" gap={1}>
              <IconButton onClick={handleFilter}>
                <Badge color="primary" variant="dot" invisible={!isFiltered}>
                  <Tune />
                </Badge>
              </IconButton>
              <Button
                variant="contained"
                onClick={handleAddButton}
                disabled={!isOnline}
              >
                <Add />
                Add
              </Button>
            </Box>
          </Box>
        </Grid>
      </Grid>
    );
  };

  useEffect(() => {
    filterBySearch();
  }, [lockState.filter.searchText]);

  return (
    <Paper elevation={3}>
      <Box p={4}>
        <Box pb={2} display="flex" justifyContent="space-between" gap={2}>
          {lockListHeader()}
          <Box display="flex"></Box>
        </Box>

        <Grid container spacing={2}>
          {locks.map((lock, index) => (
            <Grid key={index} item xs={12} sm={6} md={4} lg={3}>
              <LockCard {...lock} />
            </Grid>
          ))}
        </Grid>
      </Box>
    </Paper>
  );
}

export default LockList;
