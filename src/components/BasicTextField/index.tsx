import { TextField, TextFieldProps, useMediaQuery } from "@mui/material";
import theme from "../../theme";

function BasicTextField(props: TextFieldProps) {
  const mobileSize = useMediaQuery(theme.breakpoints.down("lg"));

  return <TextField {...props} size={mobileSize ? "small" : "medium"} />;
}

export default BasicTextField;
