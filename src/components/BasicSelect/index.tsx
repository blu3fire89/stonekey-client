import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectProps,
  useMediaQuery,
} from "@mui/material";
import theme from "../../theme";

type BasicSelectProps = {
  list: List[];
} & SelectProps;

type List = {
  value: string;
  text: string;
};

const BasicSelect = (props: BasicSelectProps) => {
  const mobileSize = useMediaQuery(theme.breakpoints.down("lg"));

  return (
    <FormControl fullWidth>
      <InputLabel id="demo-simple-select-label">{props.label}</InputLabel>
      <Select {...props} size={mobileSize ? "small" : "medium"}>
        {props.list.map((item, index) => (
          <MenuItem key={index} value={item.value}>
            {item.text}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

export default BasicSelect;
