import { Box, Typography } from "@mui/material";
import { Navigate, Outlet, useNavigate } from "react-router-dom";
import "./index.css";
import Navbar from "../components/Navbar";
import { getAccountApi, getCategoriesApi, getLoginTypesApi } from "../api/api";
import { useDispatch, useSelector } from "react-redux";
import { setLoginTypes } from "../reducers/loginTypeReducer";
import { setCategories } from "../reducers/categoryReducer";
import { useEffect, useState } from "react";
import { setAccountDetails, setAuth } from "../reducers/accountReducer";
import NoMatch from "../pages/NoMatch";
import Spinner from "../assets/chick-spinner.gif";
import { setGlobalError } from "../reducers/globalReducer";
import { refreshLocks, refreshOnlineState } from "../utils/hooks";
import { getAuthDetails } from "../api/db/dexieApi";
import { WarningAmber } from "@mui/icons-material";
import { RootState } from "../store";
import { checkOnline } from "../utils/utils";

function Layout() {
  console.log("refresh layout");
  const { isOnline } = useSelector((state: RootState) => state.global);

  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const [authErr, setAuthErr] = useState(false);
  const [isReloading, setReloading] = useState(false);

  const navigate = useNavigate();

  const fetchDatas = async () => {
    // check for auth token
    const getAuth = await getAuthDetails().then(
      (res) => res && dispatch(setAuth(res))
    );
    if (!getAuth) return setAuthErr(true);
    try {
      await getAccountApi().then((res) => {
        if (res?.success) dispatch(setAccountDetails(res.data));
        else {
          // if token expired
          if (res?.name === "TokenExpiredError") {
            dispatch(
              setGlobalError({
                type: "expired-jwt",
                text: "Your session has expired",
              })
            );
            navigate("/login");
          } else if (res?.name === "JsonWebTokenError") {
            dispatch(
              setGlobalError({
                type: "no-token",
              })
            );
          }

          // if no token or others, show error page
          setError(true);
        }
      });

      await getCategoriesApi().then(
        (res) => res && dispatch(setCategories(res))
      );
      await getLoginTypesApi().then(
        (res) => res && dispatch(setLoginTypes(res))
      );

      await refreshLocks();
      await checkOnline();
      // await getLocksApi().then((res) => res && dispatch(setLocks(res)));
      await refreshOnlineState();
    } catch (error) {
      console.log("error", error);
      setError(true);
    }
  };
  const reload = async () => {
    console.log("click");

    sessionStorage.setItem("onlineMode", "1");
    setReloading(true);
    await fetchDatas();
    setReloading(false);
  };

  const initialize = async () => {
    setLoading(true);
    await fetchDatas();
    setLoading(false);
  };

  useEffect(() => {
    initialize();
  }, []);

  const content = () => {
    switch (true) {
      case authErr:
        return <Navigate to="/login" replace />;
      case error:
        return <NoMatch />;
      case loading:
        return (
          <Box
            display="flex"
            justifyContent="center"
            height="100vh"
            alignItems="center"
          >
            <Box textAlign="center">
              <img src={Spinner} alt="loading-icon" />
              <Typography variant="h6">Loading files, please wait</Typography>
            </Box>
          </Box>
        );
      default:
        return (
          <Box display="flex">
            <Box flex={1}>
              <Navbar reload={reload} loading={isReloading} />
              <Box display="flex" width="100%" justifyContent="center">
                <Box maxWidth="lg" width="100%" p={2}>
                  {!isOnline && <OfflineWarning />}
                  <Outlet />
                </Box>
              </Box>
            </Box>
          </Box>
        );
    }
  };

  return content();
}

const OfflineWarning = () => {
  return (
    <Box display="flex" alignItems="center" gap={0.5} p={2} borderRadius="5px">
      <WarningAmber color="warning" />
      <Typography>
        No internet access found, some features have been are disabled
      </Typography>
    </Box>
  );
};

export default Layout;
