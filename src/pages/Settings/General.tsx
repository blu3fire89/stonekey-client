import { Box, Grid } from "@mui/material";
import { useState } from "react";

import { editUserApi } from "../../api/api";
import { RootState } from "../../store";
import { useSelector } from "react-redux";
import BasicButton from "../../components/BasicButton";
import BasicTextField from "../../components/BasicTextField";

function General() {
  const { firstName, lastName, emailAddress, image } = useSelector(
    (state: RootState) => state.account
  );
  const { isOnline } = useSelector((state: RootState) => state.global);

  const [input, setInput] = useState({
    firstName,
    lastName,
    emailAddress,
    image,
  });

  const [loading, setLoading] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    await editUserApi(input);
    setLoading(false);
  };

  const updateField = (e) => {
    setInput({
      ...input,
      [e.target.name]: e.target.value,
    });
  };
  return (
    <form onSubmit={handleSubmit}>
      <Grid container spacing={2}>
        <Grid item xs={12} md={6}>
          <BasicTextField
            name="firstName"
            label="First Name"
            variant="outlined"
            fullWidth
            value={input?.firstName}
            onChange={updateField}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <BasicTextField
            name="lastName"
            label="Last Name"
            variant="outlined"
            fullWidth
            value={input?.lastName}
            onChange={updateField}
          />
        </Grid>
        <Grid item xs={12}>
          <BasicTextField
            name="emailAddress"
            label="Email"
            variant="outlined"
            fullWidth
            value={input?.emailAddress}
            onChange={updateField}
          />
        </Grid>
      </Grid>
      <Box display="flex" flexDirection="row-reverse" pt={2} gap={2}>
        <BasicButton
          type="submit"
          variant="contained"
          color="primary"
          isLoading={loading}
          disabled={!isOnline}
        >
          Save
        </BasicButton>
      </Box>
    </form>
  );
}

export default General;
