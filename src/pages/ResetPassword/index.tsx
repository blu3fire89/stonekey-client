import { Box, Grid, Paper, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { Link, useSearchParams } from "react-router-dom";
import BasicButton from "../../components/BasicButton";
import PasswordInput from "../../components/PasswordInput";
import { changePasswordApi } from "../../api/api";

function ResetPassword() {
  return (
    <Box height="100vh">
      <Box
        display="flex"
        alignItems="center"
        justifyContent="center"
        height="100%"
        width="100%"
        px={2}
        flexDirection="column"
        gap={1}
      >
        <ResetPasswordForm />
      </Box>
    </Box>
  );
}

const ResetPasswordForm = () => {
  const [searchParams] = useSearchParams();
  const [resetPassId, setResetPassId] = useState(null);
  const [loading, setLoading] = useState(false);
  const [isSent, setSent] = useState(false);

  const initializeId = () => {
    searchParams.forEach((val, key) => {
      if (key === "code") {
        setResetPassId(val);
      }
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    const response = await changePasswordApi({
      code: resetPassId,
      password: input.password,
    });
    if (response?.status === 200) setSent(true);
    setLoading(false);
  };
  const [input, setInput] = useState({
    password: "",
    confirmPassword: "",
  });
  const [isError, setError] = useState(false);
  const disableBtn = !input.password || !input.confirmPassword;

  const updateField = (e) => {
    setInput({
      ...input,
      [e.target.name]: e.target.value,
    });
  };

  useEffect(() => {
    if (input.password !== input.confirmPassword) return setError(true);
    setError(false);
  }, [input]);

  useEffect(() => {
    initializeId();
    setSent(false);
  }, []);

  return (
    <Box width="100%" maxWidth="sm">
      {isSent ? (
        <SuccessScreen />
      ) : (
        <Paper elevation={3} style={{ padding: "20px" }}>
          <Typography variant="body2" pb={2} color="primary">
            RESET PASSWORD
          </Typography>
          <form onSubmit={handleSubmit}>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <PasswordInput
                  name="password"
                  label="Password"
                  variant="outlined"
                  fullWidth
                  value={input?.password}
                  onChange={updateField}
                  autoComplete="off"
                  autoCorrect="off"
                />
              </Grid>
              <Grid item xs={12}>
                <PasswordInput
                  name="confirmPassword"
                  label="Confirm Password"
                  variant="outlined"
                  fullWidth
                  value={input?.confirmPassword}
                  onChange={updateField}
                  autoComplete="off"
                  autoCorrect="off"
                />
              </Grid>
              {isError && (
                <Grid item xs={12}>
                  <Typography color="error">Inputs does not match</Typography>
                </Grid>
              )}
              <Grid item xs={12}>
                <BasicButton
                  type="submit"
                  variant="contained"
                  color="primary"
                  fullWidth
                  disabled={isError || disableBtn}
                  isLoading={loading}
                >
                  Submit
                </BasicButton>
              </Grid>
            </Grid>
          </form>
        </Paper>
      )}
      <Box pt={2} textAlign="center">
        <Typography component="p">
          Remembered your password?{" "}
          <Link to="/login">
            <Typography component="span" color="#F0C029">
              Login
            </Typography>
          </Link>
        </Typography>
      </Box>
    </Box>
  );
};

const SuccessScreen = () => {
  return (
    <Paper elevation={3} style={{ padding: "20px" }}>
      <Box>
        <Typography variant="body2" pb={2} color="primary">
          RESET PASSWORD
        </Typography>
        <Box pb={2}>
          <Typography>Account successfully updated</Typography>
        </Box>
        <Link to="/login">
          <BasicButton fullWidth variant="contained">
            Go to Login
          </BasicButton>
        </Link>
      </Box>
    </Paper>
  );
};
export default ResetPassword;
