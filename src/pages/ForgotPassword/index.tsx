import { Box, Grid, Paper, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import BasicTextField from "../../components/BasicTextField";
import { forgotPasswordApi } from "../../api/api";
import BasicButton from "../../components/BasicButton";
import { isValidEmail } from "../../utils";

function ForgotPassword() {
  return (
    <Box height="100vh">
      <Box
        display="flex"
        alignItems="center"
        justifyContent="center"
        height="100%"
        width="100%"
        px={2}
        flexDirection="column"
        gap={1}
      >
        <ForgotPasswordForm />
        {/* <SuccessScreen /> */}
      </Box>
    </Box>
  );
}

const ForgotPasswordForm = () => {
  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    const response = await forgotPasswordApi({ email: input.email });
    if (response.status === 200) setSent(true);
    setLoading(false);
  };
  const [input, setInput] = useState({
    email: "",
  });
  const [isSent, setSent] = useState(false);
  const [loading, setLoading] = useState(false);
  let isEmail = input.email && !isValidEmail(input.email);

  const updateField = (e) => {
    setInput({
      ...input,
      [e.target.name]: e.target.value,
    });
  };

  useEffect(() => {
    setSent(false);
  }, []);

  return (
    <Box width="100%" maxWidth="sm">
      {isSent ? (
        <SuccessScreen />
      ) : (
        <Paper elevation={3} style={{ padding: "20px" }}>
          <Typography variant="body2" pb={2} color="primary">
            FORGOT PASSWORD
          </Typography>
          <form onSubmit={handleSubmit}>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <BasicTextField
                  name="email"
                  label="Email"
                  variant="outlined"
                  fullWidth
                  value={input?.email}
                  onChange={updateField}
                  autoComplete="off"
                />
              </Grid>
              {isEmail && (
                <Grid item xs={12}>
                  <Typography color="error">
                    Please input valid email
                  </Typography>
                </Grid>
              )}
              <Grid item xs={12}>
                <BasicButton
                  type="submit"
                  variant="contained"
                  color="primary"
                  fullWidth
                  disabled={isEmail || !input.email}
                  isLoading={loading}
                >
                  Submit
                </BasicButton>
              </Grid>
            </Grid>
          </form>
        </Paper>
      )}

      <Box pt={2} textAlign="center">
        <Typography component="p">
          Already have an account?{" "}
          <Link to="/login">
            <Typography component="span" color="#F0C029">
              Login
            </Typography>
          </Link>
        </Typography>
      </Box>
    </Box>
  );
};

const SuccessScreen = () => {
  return (
    <Paper elevation={3} style={{ padding: "20px" }}>
      <Box>
        <Typography variant="body2" pb={2} color="primary">
          FORGOT PASSWORD
        </Typography>
        <Box pb={2}>
          <Typography>
            Check your email for reset password instructions
          </Typography>
        </Box>
        <Link to="/login">
          <BasicButton fullWidth variant="contained">
            Go to login
          </BasicButton>
        </Link>
      </Box>
    </Paper>
  );
};

export default ForgotPassword;
