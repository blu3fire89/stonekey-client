export interface CreateCategoryProps {
  title: string;
}

export interface Category {
  id?: string;
  _id: string;
  title: string;
  userId: string;
  createdAt?: Date;
  updatedAt?: Date;
}
