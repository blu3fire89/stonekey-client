export interface LoginType {
  id?: string;
  _id: string;
  title: string;
  code: string;
  passwordRequired: boolean;
}
