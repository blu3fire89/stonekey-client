export interface Account {
  id?: number;
  userId?: string;
  firstName?: string;
  lastName?: string;
  emailAddress?: string;
  image?: string;
  accountNumber?: string;
  masterKeyId?: string;
}

export interface AccountAuth {
  id?: number;
  accessToken?: string;
  masterKey?: string;
}
