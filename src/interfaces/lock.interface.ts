export type LoginTypeDetails = {
  _id: string;
  title: string;
  code: string;
  passwordRequired: boolean;
};

export type CategoryDetails = {
  _id?: string;
  createdAt?: Date;
  isDeleted?: boolean;
  title?: string;
  updatedAt?: Date;
  userId?: string;
};

export type Lock = {
  id?: string;
  _id: string;
  masterKeyId: string;
  username?: string;
  password?: string;
  title?: string;
  website?: string;
  categoryArr?: string[];
  categoryDetails?: CategoryDetails[];
  description?: string;
  favorite?: boolean;
  createdBy?: string;
  loginTypeDetails?: LoginTypeDetails;
  createdAt?: Date;
  updatedAt?: Date;
};
