import Dexie, { Table } from "dexie";
import { Lock } from "../../interfaces/lock.interface";
import { Category } from "../../interfaces/category.interface";
import { Account, AccountAuth } from "../../interfaces/account.interface";
import { LoginType } from "../../interfaces/loginType.interface";

export class MySubClassedDexie extends Dexie {
  auth!: Table<AccountAuth>;
  locks!: Table<Lock>;
  categories!: Table<Category>;
  account!: Table<Account>;
  loginTypes!: Table<LoginType>;

  constructor() {
    super("stonekeyDB");
    this.version(3).stores({
      auth: "id, accessToken, masterKey",
      locks:
        "++id, _id, masterKeyId, username, password, title, website, categoryArr, categoryDetails, description, favorite, loginTypeDetails, createdAt, updatedAt",
      categories: "++id, _id, userId, title, createdAt, updatedAt",
      account:
        "id, userId, firstName, lastName, emailAddress, image, accountNumber, masterKeyId",
      loginTypes: "++id, _id, title, code, passwordRequired",
    });
  }
}

export const db = new MySubClassedDexie();
