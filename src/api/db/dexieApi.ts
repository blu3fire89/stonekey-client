import { toast } from "react-toastify";
import { db } from "./db";
import { Lock } from "../../interfaces/lock.interface";
import { Category } from "../../interfaces/category.interface";
import { jwtDecode } from "jwt-decode";
import { Account } from "../../interfaces/account.interface";
import { LoginType } from "../../interfaces/loginType.interface";

// check if functions well
// async function isDataStale() {
//   const threshold = 5 * 60 * 1000; // 24 hours in milliseconds
//   // const threshold = 30 * 60 * 1000; // 24 hours in milliseconds
//   const lastUpdated = await db.locks.orderBy("updatedAt").last(); // Assuming you have a timestamp field
//   if (!lastUpdated) {
//     return true; // If no data is present, consider it stale
//   }
//   const currentTime = new Date().getTime();
//   return currentTime - new Date(lastUpdated.updatedAt).getTime() > threshold;
// }

// check if the storage locks is equal to fetched locks
// async function isLockListStale(locks: Lock[]) {
//   const lastUpdated = await db.locks.orderBy("updatedAt").last(); // Assuming you have a timestamp field
//   if (!lastUpdated) {
//     return true; // If no data is present, consider it stale
//   }

//   const latestLock = locks[0];
//   const latestLockTime = new Date(latestLock.updatedAt).getTime();
//   const latestLockStorageTime = new Date(lastUpdated.updatedAt).getTime();
//   return latestLockTime > latestLockStorageTime;
// }

interface ItemObj {
  _id: string;
  updatedAt?: Date;
  [key: string]: any;
}

const newItemFromList = (array1: ItemObj[], array2: ItemObj[]) => {
  const newItems = [];
  const staleItems = [];
  const deletedItemIds = [];
  array1.forEach((item1) => {
    const matchingItem = array2.find((item2) => item1._id === item2._id);
    if (!matchingItem) {
      newItems.push(item1);
    } else if (
      new Date(item1.updatedAt).getTime() !==
      new Date(matchingItem.updatedAt).getTime()
    ) {
      const matchedObj = {
        key: matchingItem.id,
        changes: item1,
      };
      staleItems.push(matchedObj);
    }
  });
  array2.forEach((item2) => {
    const found = array1.some((item1) => item1._id === item2._id);
    if (!found) {
      deletedItemIds.push(item2.id);
    }
  });

  return {
    newItems,
    staleItems,
    deletedItemIds,
  };
};

// ---------------------------------->

export const getAuthDetails = async () => {
  return await db.auth.where("id").equals(0).first();
};

export const storeAuthDetails = async (props) => {
  const { accessToken, masterKey } = props;
  await db.auth.clear();
  await db.auth.add({ id: 0, accessToken, masterKey });
};

export const deleteAuthDetails = async () => {
  await db.auth.clear();
};

export const logoutAccount = async () => {
  await db.auth.update(0, { accessToken: null });
  // await db.account.clear();
};

export const deleteAccount = async () => {
  db.auth.clear();
  db.account.clear();
};

export const storeLocks = async (props: Lock[]) => {
  try {
    if (!props.length) {
      return {
        success: false,
        message: "Unable to store lock",
      };
    }
    const dbLocks = await db.locks.toArray();

    if (!dbLocks.length) {
      // not locks on storage
      await db.locks.bulkPut(props);
      return {
        success: true,
        message: "Save lock offline",
      };
    } else {
      // update locks on storage
      const { newItems, staleItems, deletedItemIds } = newItemFromList(
        props,
        dbLocks
      );
      if (newItems.length) await db.locks.bulkPut(newItems); // add new locks on storage
      if (staleItems.length) await db.locks.bulkUpdate(staleItems); // update locks on storage
      if (deletedItemIds.length) await db.locks.bulkDelete(deletedItemIds); // delete locks on storage
    }

    return {
      success: true,
      message: "Save lock offline",
    };
  } catch (error) {
    toast.error("Unable to add lock");
    return {
      success: false,
      message: "Unable to store lock",
    };
  }
};

export const getLocks = async () => {
  const errMessage = "Unable to get locks";
  try {
    const account = await db.account.where("id").equals(0).first();

    const locks = await db.locks
      .where("masterKeyId")
      .equals(account.masterKeyId)
      .toArray();
    return {
      data: locks,
      success: true,
      message: "Save lock offline",
    };
  } catch (error) {
    toast.error(errMessage);
    return {
      success: false,
      message: errMessage,
    };
  }
};

export const storeCategories = async (props: Category[]) => {
  try {
    if (!props.length) {
      return {
        success: false,
        message: "Unable to store categories",
      };
    }
    const dbCategories = await db.categories.toArray();

    if (!dbCategories.length) {
      // not locks on storage
      await db.categories.bulkPut(props);
      return {
        success: true,
        message: "Save categories offline",
      };
    } else {
      // update locks on storage
      const { newItems, staleItems, deletedItemIds } = newItemFromList(
        props,
        dbCategories
      );
      if (newItems.length) await db.locks.bulkPut(newItems); // add new locks on storage
      if (staleItems.length) await db.locks.bulkUpdate(staleItems); // update locks on storage
      if (deletedItemIds.length) await db.locks.bulkDelete(deletedItemIds); // delete locks on storage
    }

    return {
      success: true,
      message: "Save lock offline",
    };
  } catch (error) {
    toast.error("Unable to add lock");
    return {
      success: false,
      message: "Unable to store lock",
    };
  }
};

export const getCategories = async () => {
  const errMessage = "Unable to get categories";
  try {
    const auth = await db.auth.where("id").equals(0).first();
    const decodedToken = jwtDecode(auth.accessToken) as {
      id: string;
      name: string;
      isAdmin: string;
    };

    const categories = await db.categories
      .where("userId")
      .equals(decodedToken?.id)
      .toArray();
    return {
      data: categories,
      success: true,
      message: "Save lock offline",
    };
  } catch (error) {
    toast.error(errMessage);
    return {
      success: false,
      message: errMessage,
    };
  }
};

export const storeAccount = async (props: Account) => {
  try {
    if (!props) {
      return {
        success: false,
        message: "Unable to store account",
      };
    }
    const account = {
      ...props,
      id: 0,
    };
    const dbAccount = await db.account.toArray();

    if (!dbAccount.length) {
      // not locks on storage
      await db.account.add(account);
      return {
        success: true,
        message: "Save account offline",
      };
    } else {
      // update locks on storage
      await db.account.clear();
      await db.account.add(account);
    }

    return {
      success: true,
      message: "Save lock offline",
    };
  } catch (error) {
    toast.error("Unable to store account");
    return {
      success: false,
      message: "Unable to store account",
    };
  }
};

export const getAccount = async () => {
  const errMessage = "Unable to get locks";
  try {
    const account = await db.account.toArray();
    if (!account.length)
      return {
        success: false,
        message: "No account found",
      };
    return {
      data: account[0],
      success: true,
      message: "Save lock offline",
    };
  } catch (error) {
    toast.error(errMessage);
    return {
      success: false,
      message: errMessage,
    };
  }
};

export const storeLoginTypes = async (props: LoginType[]) => {
  try {
    if (!props.length) {
      return {
        success: false,
        message: "Unable to store lock",
      };
    }
    const dbLoginTypes = await db.loginTypes.toArray();

    if (!dbLoginTypes.length) {
      // not locks on storage
      await db.loginTypes.bulkPut(props);
      return {
        success: true,
        message: "Save lock offline",
      };
    } else {
      // if storage loginType list length is not equal to fetched loginType list len
      if (dbLoginTypes.length !== props.length) {
        await db.loginTypes.clear();
        await db.loginTypes.bulkPut(props);
      }
    }

    return {
      success: true,
      message: "Save lock offline",
    };
  } catch (error) {
    toast.error("Unable to add lock");
    return {
      success: false,
      message: "Unable to store lock",
    };
  }
};

export const getLoginTypes = async () => {
  const errMessage = "Unable to get login types";
  try {
    const loginTypes = await db.loginTypes.toArray();
    return {
      data: loginTypes,
      success: true,
      message: "Got login types offline",
    };
  } catch (error) {
    toast.error(errMessage);
    return {
      success: false,
      message: errMessage,
    };
  }
};
