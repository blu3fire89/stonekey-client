import { routeGetApi, routePostApi, routeUpdateApi } from ".";
import { CreateCategoryProps } from "../interfaces/category.interface";
import { checkOnline } from "../utils/utils";
import {
  getAccount,
  getCategories,
  getLocks,
  getLoginTypes,
  storeAccount,
  storeCategories,
  storeLocks,
  storeLoginTypes,
} from "./db/dexieApi";
import { toast } from "react-toastify";

interface GenericResponse {
  success?: boolean;
  message?: string;
}

interface CreateLockProps {
  logo?: string;
  title?: string;
  loginTypeCode?: string;
  username?: string;
  password?: string;
  category?: string;
  website?: string;
  description?: string;
  categoryArr?: any[];
  categoryDetails?: any[];
}

export const loginApi = async (input): Promise<GenericResponse> => {
  if (await checkOnline()) {
    const response = await routePostApi("/login", input);
    if (response.success) return response;
  }
};

// locks
export const getLocksApi = async () => {
  let locks;
  if (await checkOnline()) {
    const response = await routeGetApi("/viewlocks");
    locks = response?.data;
    if (response?.data.length) await storeLocks(response?.data);
  } else {
    const { data, success } = await getLocks();
    if (success) locks = data;
  }
  return locks;
};

export const createLockApi = async (props: CreateLockProps) => {
  if (await checkOnline()) {
    return await routePostApi("/createlock", props);
  } else {
    toast.error("No internet access found");
  }
};

export const editLockApi = async (id, params) => {
  if (await checkOnline()) {
    await routeUpdateApi({
      apiRoute: "/updatelock/" + id,
      params,
    });
  } else {
    toast.error("No internet access found");
  }
};
export const deleteLockApi = async (id): Promise<GenericResponse> => {
  if (await checkOnline()) {
    const deleteLockRes = await routeUpdateApi({
      apiRoute: "/archivelock/" + id,
    });
    if (deleteLockRes.success) return deleteLockRes;
  } else {
    toast.error("No internet access found");
  }
};

// categories
export const getCategoriesApi = async () => {
  let categories;
  if (await checkOnline()) {
    const response = await routeGetApi("/displaycategories");
    categories = response?.data;
    if (response?.data?.length) await storeCategories(response?.data);
  } else {
    const { data, success } = await getCategories();
    if (success) categories = data;
  }
  return categories;
};
export const createCategoryApi = async (props: CreateCategoryProps) => {
  if (await checkOnline()) {
    await routePostApi("/createcategory", props);
  } else {
    toast.error("No internet access found");
  }
};
export const editCategoryApi = async (id, params) => {
  if (await checkOnline()) {
    await routeUpdateApi({
      apiRoute: "/updatecategory/" + id,
      params,
    });
  } else {
    toast.error("No internet access found");
  }
};
export const deleteCategoryApi = async (id): Promise<GenericResponse> => {
  if (await checkOnline()) {
    const deleteLockRes = await routeUpdateApi({
      apiRoute: "/archivecategory/" + id,
    });
    if (deleteLockRes.success) return deleteLockRes;
  } else {
    toast.error("No internet access found");
  }
};

// login types
export const getLoginTypesApi = async () => {
  let loginTypes;
  if (await checkOnline()) {
    const response = await routeGetApi("/displaylogintype");
    // return response
    loginTypes = response?.data;
    if (response?.data.length) await storeLoginTypes(response.data);
  } else {
    const { data, success } = await getLoginTypes();
    if (success) loginTypes = data;
  }
  return loginTypes;
};

// user
export const editUserApi = async (params) => {
  if (await checkOnline()) {
    await routeUpdateApi({
      apiRoute: "/updateaccount",
      params,
    });
  } else {
    toast.error("No internet access found");
  }
};

export const getAccountApi = async () => {
  let account;
  if (await checkOnline()) {
    const response = await routeGetApi("/viewuser");
    account = {
      ...response,
      ...response?.data,
      name: response?.name,
    };
    if (response?.data) {
      await storeAccount(response.data);
    }
  } else {
    const storageRes = await getAccount();
    account = {
      ...storageRes,
      ...storageRes?.data,
    };
  }
  return account;
};

export const editPasswordApi = async (params): Promise<GenericResponse> => {
  if (await checkOnline()) {
    return await routeUpdateApi({
      apiRoute: "/updatepassword",
      params,
    });
  } else {
    toast.error("No internet access found");
  }
};

export const deleteAccountApi = async (id): Promise<GenericResponse> => {
  if (await checkOnline()) {
    return await routeUpdateApi({
      apiRoute: "/archiveuser/" + id,
    });
  } else {
    toast.error("No internet access found");
  }
};

export const downloadLockApi = async () => {
  if (await checkOnline()) {
    return await routeGetApi("/lockstocsv");
  } else {
    toast.error("No internet access found");
  }
};

export const changePasswordApi = async (params) => {
  if (await checkOnline()) {
    return await routePostApi("/changepassword", params);
  } else {
    toast.error("No internet access found");
  }
};
export const forgotPasswordApi = async (params) => {
  if (await checkOnline()) {
    return await routePostApi("/forgotpassword", params);
  } else {
    toast.error("No internet access found");
  }
};
